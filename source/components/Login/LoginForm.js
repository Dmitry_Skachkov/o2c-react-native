import React, {Component} from 'react';
import {AppRegistry, View, TextInput, TouchableOpacity, Text, Alert, ActivityIndicator} from 'react-native';
import Notification from 'react-native-in-app-notification';
import {MaterialIndicator} from 'react-native-indicators';

export default class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            usernameColorStyle: 'blur',
            passwordColorStyle: 'blur',
            activityIndicator: false,
        };
        this._onLoginPress = this._onLoginPress.bind(this);
        // this._onFocus = this._onFocus.bind(this);
        // this._onBlur = this._onBlur.bind(this);
    }

    render() {
        return (
          <View style={styles.container}>
              <TextInput
                style={[styles.input, styles[this.state.usernameColorStyle]]}
                placeholder='IBM ID'
                returnKeyType='next'
                onSubmitEditing={() => this.password.focus()}
                keyboardType='email-address'
                autoCapitalize='none'
                autoCorrect={false}
                underlineColorAndroid='transparent'
                clearButtonMode={'always'}
                onChangeText={(username) => this.setState({username: username})}
                onFocus={() => {
                    this.setState({
                        usernameColorStyle: 'focus'
                    })
                }}
                onBlur={() => {
                    this.setState({
                        usernameColorStyle: 'blur'
                    })
                }}
              />
              <TextInput
                style={[styles.input, styles[this.state.passwordColorStyle]]}
                placeholder='PASSWORD'
                secureTextEntry
                returnKeyType='go'
                ref={(input) => this.password = input}
                onChangeText={(password) => this.setState({password: password})}
                onFocus={() => {
                    this.setState({
                        passwordColorStyle: 'focus'
                    })
                }}
                onBlur={() => {
                    this.setState({
                        passwordColorStyle: 'blur'
                    })
                }}
                underlineColorAndroid='transparent'
              />
              <TouchableOpacity
                onPress={this._onLoginPress}
                style={styles.buttonContainer}>
                  {
                      (this.state.activityIndicator) ? (
                        <MaterialIndicator
                          animating={true}
                          color='white'
                        />
                      ) : (
                        <Text style={styles.buttonText}>Sign in</Text>
                      )
                  }
              </TouchableOpacity>
          </View>
        );
    }

    // _onFocus() {
    //     this.setState({
    //         inputColorStyle: 'focus',
    //     });
    // }
    //
    // _onBlur() {
    //     this.setState({
    //         inputColorStyle: 'blur',
    //     });
    // }

    _onLoginPress() {
        this.setState({activityIndicator: true});

        let username = this.state.username;
        let password = this.state.password;

        fetch('http://172.20.15.122:9080/OrderToCash/Logon?username=' + username + '&password=' + password)
          .then((response) => {
              console.log(response);
              if (response.status === 200) {
                  const { navigate } = this.props.navigation;
                  navigate('FBMStatuses');
              }
              this.setState({activityIndicator: false});
          })
          .catch((error) => {
              this.setState({activityIndicator: false});
              console.log(error);
          });
    }
}

const styles = {
    container: {
        padding: 10,
    },
    input: {
        height: 50,
        marginBottom: 10,
        color: 'black',
        paddingHorizontal: 10
    },
    buttonContainer: {
        height: 60,
        justifyContent: 'center',
        backgroundColor: '#2a77ac',
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontWeight: '700'
    },
    focus: {
        backgroundColor: '#FFF',
        borderWidth: 1,
    },
    blur: {
        backgroundColor: '#E0E0E0',
    }
};

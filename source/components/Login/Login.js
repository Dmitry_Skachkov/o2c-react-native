import React, {Component} from 'react';
import {AppRegistry, View, Image, Text, KeyboardAvoidingView, TextInput} from 'react-native';
import LoginForm from './LoginForm'

export default class Login extends Component {
    render() {
        return (
          <View style={styles.container}>
              <View style={styles.titleContainer}>
                  <Text style={styles.title}>Sign in Order To Cash</Text>
              </View>
              <LoginForm style={styles.form}/>
          </View>
        );
    }
};

const styles = {
      container: {
          flex: 1,
          paddingBottom: 10,
          backgroundColor: '#fff',
      },
      titleContainer: {
          flex: 1,
          justifyContent: 'center',
      },
      title: {
          flexWrap: 'wrap',
          alignSelf: 'center',
          justifyContent: 'center',
          fontSize: 20,
          fontWeight: '700',
      },
  }
;

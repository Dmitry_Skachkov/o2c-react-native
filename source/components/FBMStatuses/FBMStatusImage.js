import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';

export default class FBMStatusImage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const image = this.props.image;
        const count = this.props.count;
        const isVisible = this.props.isVisibleCount;
        return (
            <View style={style.container}>
                <Image
                    source={image}
                    style={style.image}
                />
                {isVisible ?
                    (
                        <View
                            style={style.shape}>
                            <Text style={style.text}>{count}</Text>
                        </View>
                    )
                    : null}
            </View>
        )
    }
}

const circleRadius = 10;
const imageSize = 48;

const documentStyle = {
    left: -((Math.sqrt(2) - 1) * imageSize / 2 + circleRadius) +3,
    top: (Math.sqrt(2) - 1) * imageSize / 2 - circleRadius,
};

console.log(documentStyle.top);

let style = {
    container: {
        //borderWidth: 0.5,
        //width: imageSize,
        //height: imageSize + 10,
        flexDirection: 'row',
    },
    text: {
        fontSize: 10,
        fontWeight: '800',
        color: '#fff',
        alignSelf: 'center'
    },
    shape: {
        //flex: 1,
        //borderWidth: 1,
        left: documentStyle.left,
        top: documentStyle.top,
        height: 2 * circleRadius,
        width: 2 * circleRadius,
        borderRadius: circleRadius,
        backgroundColor: 'red',
        justifyContent: 'center',
    },
    image: {
        alignSelf: 'flex-end',
        height: imageSize,
        width: imageSize,
    },
};

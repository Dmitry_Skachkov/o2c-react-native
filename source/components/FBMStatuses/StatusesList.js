import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FBMStatusImage from './FBMStatusImage';

export default class StatusesList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            statuses: [],
            time: '',
            error: ''
        };
    }

    render() {
        const statuses = this.props.statuses;
        console.log(this.props.navigation);
        let navigation = this.props.navigation;
        if (statuses.length === 0) {
            return null
        }
        ////// Temporary //////
        let i = 0;
        ///////////////////////
        let statusesList = statuses.map(status => {
            if (status == null) {
                status = {};
                status.name = "It's a big problem";
                status.statusColor = 'red'
                status.errorDocNumb = 0
            }
            let position = status.name.indexOf(' system status');
            let name = position === -1 ? status.name : status.name.substring(0, position);
            let documentCount = status.errorDocNumb;
            const images = {
                green: require('../../../images/success.png'),
                red: require('../../../images/error.png')
            };
            let statusColor = status.statusColor === 'green' ? 'green' : 'red';

            console.log(name + '    ' + statusColor);

            return (
                <TouchableOpacity
                  ////// Temporary //////
                  key={i++}
                  ///////////////////////
                  style={style.status}
                  onPress={() => navigation.navigate('SystemInfo', {name: name, status: statusColor})}>
                    <FBMStatusImage
                        image={images[statusColor]}
                        count={documentCount}
                        //isVisibleCount={true}
                        isVisibleCount={statusColor === 'green'}
                    />
                    <Text style={style.text}>{name}</Text>
                </TouchableOpacity>);
        });
        return (<View>{statusesList}</View>)
    }
};

const style = {
    status: {
        flexDirection: 'row',
        marginTop: 20,
        marginLeft: 20,
    },
    text: {
        fontSize: 20,
        fontWeight: '700',
        marginLeft: 15,
        paddingVertical: 10
    },
};

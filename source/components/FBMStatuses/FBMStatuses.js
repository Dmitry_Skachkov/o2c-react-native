import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import StatusesList from './StatusesList'

export default class FBMStatuses extends Component {

    navigationOptions: {
        title: 'FBM statuses',
        titleStyle: {
            textSelf: 'center',
        },
        headerLeft: null,
    }

    constructor(props) {
        super(props);

        this.state = {
            statuses: [],
            time: '',
            error: ''
        };
        this._getFBMStatuses = this._getFBMStatuses.bind(this);
        this._getFBMStatuses();
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
          <View style={styles.container}>
              <StatusesList
                statuses={this.state.statuses}
                navigation={this.props.navigation}/>
              <Text style={styles.error}>{this.state.error}</Text>
          </View>
        );
    }

    _getFBMStatuses() {
        let body = [];
        if (this.state.error === '') {
            let encodedProperty = encodeURIComponent('time');
            let encodedValue = encodeURIComponent(this.state.time);
            body.push(encodedProperty + "=" + encodedValue);
        }
        body = body.join("&");

        fetch('http://172.20.15.122:9080/OrderToCash/mobile/FBMStatus', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: body
        })
          .then(response => {
              console.log(response);
              //TODO handle error
              return response.json();
          })
          .then(statuses => {
              console.log(statuses.date);
              //TODO reduce response.body
              this.setState({statuses: statuses.statuses, time: statuses.date, error: ''});
              this._getFBMStatuses();
          })
          .catch(error => {
              console.log(error);
              this.setState({error: error.message + ' ' + error.code});
              setTimeout(this._getFBMStatuses, 5000);
          });
    }
};

let styles = {
    container: {
        flex: 1,
        backgroundColor: '#F4F4F2',
    },
    error: {
        fontSize: 15,
        fontWeight: '600',
        marginTop: 30,
        marginLeft: 20,
    }
};

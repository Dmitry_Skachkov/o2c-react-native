import {
    StackNavigator,
    DrawerNavigator,
} from 'react-navigation';

import Login from './Login/Login'
import FBMStatuses from './FBMStatuses/FBMStatuses';
import SystemInfo from './SystemInfo/SystemInfo';

export const FBMStatusesNavigator = StackNavigator({
    FBMStatuses: {
        screen: FBMStatuses,
        navigationOptions: {
            title: 'FBM statuses',
            titleStyle: {
                textSelf: 'center',
            },
            headerLeft: null,
        }
    },
    SystemInfo: {
        screen: SystemInfo,
        navigationOptions: {
            title: 'System information',
            titleStyle: {
                textSelf: 'center',
            }
        }
    }
});

export const AppNavigation = DrawerNavigator({
    FBMStatusesNavigator: {
        screen: FBMStatusesNavigator,
        navigationOptions: {
            drawerLabel: 'FBMStatuses',
            header: false,
        }
    }
});

export const LoginNavigation = StackNavigator({
    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
    },
    AppNavigation: {
        screen: AppNavigation,
    }
});

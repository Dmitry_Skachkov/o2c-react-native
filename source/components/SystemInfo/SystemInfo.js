import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';

export default class SystemInfo extends Component {
    
    navigationOptions: {
        title: 'System information',
        titleStyle: {
            textSelf: 'center',
        }
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { params } = this.props.navigation.state;
        return (
          <View>
              <Text>{params.name}</Text>
              <Text>{params.status}</Text>
          </View>
        )
    }
}
